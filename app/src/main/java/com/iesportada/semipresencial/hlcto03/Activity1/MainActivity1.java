package com.iesportada.semipresencial.hlcto03.Activity1;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import com.iesportada.semipresencial.hlcto03.Activity1.Network.Conection;
import com.iesportada.semipresencial.hlcto03.Activity1.Network.Result;
import com.iesportada.semipresencial.hlcto03.R;
import com.iesportada.semipresencial.hlcto03.databinding.ActivityMain1Binding;
import com.squareup.picasso.Picasso;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Rafa Narvaiza
 *
 * APP designed to implement several asynctask methods in order to shown url containing files on a webview and saving then on SDCard.
 */

public class MainActivity1 extends AppCompatActivity implements View.OnClickListener {

    private ActivityMain1Binding binding;
    private static final int REQUEST_CONNECT = 1;
    private static final int REQUEST_WRITE = 1;
    long inicio, fin;
    private URL url;
    private String fileName;
    TaskAsync taskasync;
    private int value = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMain1Binding.inflate(getLayoutInflater());
        binding.webView.setVisibility(View.INVISIBLE);
        binding.imageView.setVisibility(View.INVISIBLE);
        setContentView(R.layout.activity_main1);
        View view = binding.getRoot();
        setContentView(view);
        binding.buttonDownload.setOnClickListener((View.OnClickListener) this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WebView.enableSlowWholeDocumentDraw();
        }
    }

    /**
     * Implement onClickListener method to initialize.
     * @param v
     */

    @Override
    public void onClick(View v){
        try {
            setUrl(new URL(binding.editTextUrl.getText().toString()));
            showContent(getUrl());
            urlFilter();
            save();
        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
            showToast(e.getMessage());
        }
    }

    /**
     * Method designed to choose the kind of file looking to url name.
     * @return
     */
    public int urlFilter(){

        if (url.toString().endsWith("html") ){
            binding.imageView.setVisibility(View.INVISIBLE);
            setValue(1);
            setFileName("file.html");
            binding.webView.setVisibility(View.VISIBLE);
        }if (url.toString().endsWith("jpg") || url.toString().endsWith("jpeg")){
            binding.webView.setVisibility(View.INVISIBLE);
            binding.imageView.setVisibility(View.VISIBLE);
            setValue(2);
            setFileName("file.jpg");
        }if (url.toString().endsWith("png") || url.toString().endsWith("PNG")){
            binding.webView.setVisibility(View.INVISIBLE);
            binding.imageView.setVisibility(View.VISIBLE);
            setValue(3);
            setFileName("file.png");
        } if (url.toString().endsWith("txt")){
            binding.imageView.setVisibility(View.INVISIBLE);
            binding.webView.setVisibility(View.VISIBLE);
            setValue(4);
            setFileName("file.txt");
        }
        return getValue();
    }

    /**
     * Requesting permissions on SD and checking if its available.
     */

    public void save() {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String permissions = Manifest.permission.WRITE_EXTERNAL_STORAGE;

        if (ActivityCompat.checkSelfPermission(this, permissions) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{permissions}, REQUEST_WRITE);

        } else {

            write();
        }
    }

    /**
     * Filtering methods owing to his kind of file.
     * Each method will implement an asynchronous task to launch the I/O, buffering and writting.
     */

    private void write() {
        if(Memory.enableWrite()){
            switch(urlFilter()){
                case 1:
                    saveHTMLFile(this, getUrl().toString());
                    break;
                case 2:
                    saveJPGImage(this, getUrl().toString());
                    break;
                case 3:
                    savePNGImage(this, getUrl().toString());
                    break;
                case 4:
                    saveTXT(this, getUrl().toString());
                    break;
            }

        } else{
            showToast("External SD is not available" );

        }
    }


    private void showContent(URL url){
        taskasync = new TaskAsync(this);
        taskasync.execute(url);
    }

    /**
     * Toast generator for non static calls.
     * @param s
     */
    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }


    /**
     * Method which implements an asynchronous task to show on a webview several files.
     *
     */

    public class TaskAsync extends AsyncTask<URL, Void, Result>{
        private Context context;
        private ProgressDialog progress;

        public TaskAsync(Context context){
            this.context=context;
        }

        @Override
        protected Result doInBackground(URL... urls) {
            Result result;
            try {
                result = Conection.conectJava(urls[0]);
            } catch (IOException e) {
                Log.e("Connection error: ", e.getMessage());
                result = new Result();
                result.setCode(500);
                result.setMessage(e.getMessage());
            }
            return result;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(context);
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setMessage("Connecting ...");
            progress.setCancelable(true);
            progress.setOnCancelListener(dialog -> TaskAsync.this.cancel(true));
            progress.show();
        }


        @Override
        protected void onPostExecute(Result result) {
            super.onPostExecute(result);
            progress.dismiss();
            if (result.getCode() == HttpURLConnection.HTTP_OK){
                loadFilesFromUrl();

            }else{
                showToast(result.getMessage());
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progress.dismiss();
            showToast("Canceled");
        }
    }

    /**
     * Method called from onPostExecute to shown files owing to his kind of file.
     */

    public void loadFilesFromUrl(){

        if (getValue()==2 || getValue()==3){
            binding.webView.setVisibility(View.INVISIBLE);
            binding.imageView.setVisibility(View.VISIBLE);
            Picasso.get().load(getUrl().toString()).into(binding.imageView);
        }
        if (getValue()==1 || getValue()==4){
            binding.imageView.setVisibility(View.INVISIBLE);
            binding.webView.setVisibility(View.VISIBLE);
            binding.webView.loadUrl(String.valueOf(url));
        }
    }

    /**
     * Asynchron task which loads a JPG to save on SD.
     * @param context
     * @param MyUrl
     */

    private static void saveJPGImage(final Context context, final String MyUrl){
        final ProgressDialog progress = new ProgressDialog(context);
        class SaveThisImage extends AsyncTask<Void, Void, Void> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress.setTitle("Processing");
                progress.setMessage("Please Wait...");
                progress.setCancelable(false);
                progress.show();
            }
            @Override
            protected Void doInBackground(Void... arg0) {
                try{

                    File sdCard = Environment.getExternalStorageDirectory();
                    @SuppressLint("DefaultLocale") String fileName = String.format("%d.jpg", System.currentTimeMillis()); //Naming the file
                    File dir = new File(sdCard.getAbsolutePath() + "/HLCDownloads"); //Choosing the folder to place the file
                    dir.mkdirs();
                    final File myImageFile = new File(dir, fileName); // Create image file
                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(myImageFile);
                        Bitmap bitmap = Picasso.get().load(MyUrl).get();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);

                        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                        intent.setData(Uri.fromFile(myImageFile));
                        context.sendBroadcast(intent);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if(progress.isShowing()){
                    progress.dismiss();
                }
                Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
            }
        }
        SaveThisImage shareimg = new SaveThisImage();
        shareimg.execute();
    }

    /**
     * Asynchron task which loads a PNG to save on SD.
     * @param context
     * @param MyUrl
     */

    private static void savePNGImage(final Context context, final String MyUrl){
        final ProgressDialog progress = new ProgressDialog(context);
        class SaveThisImage extends AsyncTask<Void, Void, Void> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress.setTitle("Processing");
                progress.setMessage("Please Wait...");
                progress.setCancelable(false);
                progress.show();
            }
            @Override
            protected Void doInBackground(Void... arg0) {
                try{

                    File sdCard = Environment.getExternalStorageDirectory();
                    @SuppressLint("DefaultLocale") String fileName = String.format("%d.png", System.currentTimeMillis());
                    File dir = new File(sdCard.getAbsolutePath() + "/HLCDownloads");
                    dir.mkdirs();
                    final File myImageFile = new File(dir, fileName); // Create image file
                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(myImageFile);
                        Bitmap bitmap = Picasso.get().load(MyUrl).get();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                        intent.setData(Uri.fromFile(myImageFile));
                        context.sendBroadcast(intent);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if(progress.isShowing()){
                    progress.dismiss();
                }
                Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
            }
        }
        SaveThisImage shareimg = new SaveThisImage();
        shareimg.execute();
    }

    /**
     * Asynchron task which loads a HTML to save on SD.
     * @param context
     * @param MyUrl
     */

    private static void saveHTMLFile(final Context context, final String MyUrl){

        class DownloadTask extends com.iesportada.semipresencial.hlcto03.Activity1.DownloadTask {


            @Override
            protected String doInBackground(String... urls) {

                File sdCard = Environment.getExternalStorageDirectory();
                @SuppressLint("DefaultLocale") String fileName = String.format("%d.html", System.currentTimeMillis());
                File dir = new File(sdCard.getAbsolutePath() + "/HLCDownloads");
                dir.mkdirs();
                final File myHTMLFile = new File(dir, fileName);
                try {
                    URL url = new URL(MyUrl);
                    InputStream in = new BufferedInputStream(url.openStream());
                    OutputStream out = new FileOutputStream(myHTMLFile);
                    byte data[] = new byte[1024];
                    int count;
                    try{
                        while ((count = in.read(data)) != -1){
                            out.write(data, 0, count);
                        }
                    }catch (MalformedURLException e){
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    out.flush();
                    out.close();
                    in.close();
                }
                catch (IOException e) {
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                return null;

            }

        }
        DownloadTask dt = new DownloadTask();
        dt.execute();

    }


    /**
     * Asynchron task which loads a TXT to save on SD.
     * @param context
     * @param MyUrl
     */

    private static void saveTXT(final Context context, final String MyUrl){
        final ProgressDialog progress = new ProgressDialog(context);


        class SaveThisTxt extends AsyncTask<Void, Void, Void>{


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress.setTitle("Processing");
                progress.setMessage("Please Wait...");
                progress.setCancelable(false);
                progress.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                File sdCard = Environment.getExternalStorageDirectory();
                @SuppressLint("DefaultLocale") String fileName = String.format("%d.txt", System.currentTimeMillis());
                File dir = new File(sdCard.getAbsolutePath() + "/HLCDownloads");
                dir.mkdirs();
                final File myTxtFile = new File(dir, fileName);
                FileOutputStream fos;
                OutputStreamWriter osw;
                try {
                    URL url = new URL(MyUrl);
                    BufferedReader bufferReader = new BufferedReader(new InputStreamReader(url.openStream()));
                    String StringBuffer;
                    String stringText = "";
                    while ((StringBuffer = bufferReader.readLine()) != null) {
                        stringText += StringBuffer;
                    }
                    bufferReader.close();
                    fos = new FileOutputStream(myTxtFile);
                    osw = new OutputStreamWriter(fos);
                    osw.write(stringText);
                    osw.flush();
                    osw.close();
                    fos.close();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if(progress.isShowing()){
                    progress.dismiss();
                }
                Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
            }
        }
        SaveThisTxt stt = new SaveThisTxt();
        stt.execute();
    }

    public URL getUrl() {
        return url;
    }
    public void setUrl(URL url) {
        this.url = url;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }
}