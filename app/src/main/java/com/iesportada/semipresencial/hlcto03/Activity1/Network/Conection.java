package com.iesportada.semipresencial.hlcto03.Activity1.Network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Conection {

    public static Result conectJava(URL url) throws IOException {
        HttpURLConnection urlConnection = null;
        int response = 500;
        Result result = new Result();

        urlConnection = (HttpURLConnection) url.openConnection();
        response = urlConnection.getResponseCode();
        result.setCode(response);
        if(response == HttpURLConnection.HTTP_OK){
            result.setContent(read(urlConnection.getInputStream()));
        } else {
            result.setMessage("Error en el acceso a la web: " + String.valueOf(response));
        }
        urlConnection.disconnect();

        return result;
    }

    private static String read(InputStream inputStream) throws IOException{
        BufferedReader in;
        String line;
        StringBuilder miString = new StringBuilder();

        in = new BufferedReader((new InputStreamReader(inputStream)), 32000);
        while((line = in.readLine()) != null){
            miString.append(line);
        }
        in.close();

        return miString.toString();
    }

}
