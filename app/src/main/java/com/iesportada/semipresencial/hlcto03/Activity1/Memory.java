package com.iesportada.semipresencial.hlcto03.Activity1;

import android.os.Environment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Memory {


    public static boolean writeOnExt(String file, String s) throws IOException {
        File newFile;
        File sd = Environment.getExternalStorageDirectory();
        newFile = (new File(sd.getAbsolutePath(), file));
        return write(newFile, s);
    }
    private static boolean write(File myFile, String s) throws IOException {
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        BufferedWriter bw = null;
        boolean ok = true;

        fos = new FileOutputStream(myFile);
        osw= new OutputStreamWriter(fos);
        bw = new BufferedWriter(osw);
        bw.write(s);
        bw.close();

        return ok;
    }



    /*
    public static String mostrarPropiedades(File fichero) {
        SimpleDateFormat formato = null;
        StringBuilder texto = new StringBuilder();

        if (fichero.exists()){
            texto.append("Nombre: " + fichero.getName() + '\n');
            texto.append("Ruta: " + fichero.getAbsolutePath() + '\n');
            texto.append("Tamaño: " + Long.toString(fichero.length()) + '\n');
            formato = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.getDefault());
            texto.append("Fecha: " + formato.format(new Date(fichero.lastModified())));
        }else{
            texto.append("No existe el fichero");
        }
        return texto.toString();
    }
    public static String mostrarPropiedadesExterna(String fichero) {
        File miFichero, tarjeta;
        tarjeta = Environment.getExternalStorageDirectory();
        miFichero = new File(tarjeta.getAbsolutePath(), fichero);

        return mostrarPropiedades(miFichero);
    }

     */
    public static boolean enableWrite() {
        boolean write = false;
        String status = Environment.getExternalStorageState();
        if(status.equals(Environment.MEDIA_MOUNTED)){
            write = true;
        }
        return write;
    }
    public static boolean enableRead() {
        boolean read = false;
        String status = Environment.getExternalStorageState();
        if(status.equals(Environment.MEDIA_MOUNTED)|| status.equals(Environment.MEDIA_MOUNTED_READ_ONLY)){
            read = true;
        }
        return read;

    }
    public static String readSDExt(String file) throws IOException {

        File myFile;
        File sd = Environment.getExternalStorageDirectory();
        myFile = new File(sd.getAbsolutePath(), file);
        return read(myFile);


    }
    private static String read(File file) throws IOException {
        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;

        String s;
        StringBuilder chain = new StringBuilder();

        fis = new FileInputStream(file);
        isr = new InputStreamReader(fis);
        br = new BufferedReader(isr);

        while((s= br.readLine()) != null){
            chain.append(s).append('\n');
        }
        br.close();
        return chain.toString();
    }

}
