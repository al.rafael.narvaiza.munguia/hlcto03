package com.iesportada.semipresencial.hlcto03;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.iesportada.semipresencial.hlcto03.Activity1.MainActivity1;
import com.iesportada.semipresencial.hlcto03.Activity2.MainActivity2;
import com.iesportada.semipresencial.hlcto03.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(R.layout.activity_main);

        View view = binding.getRoot();
        setContentView(view);
        binding.Activity1Button.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this,MainActivity1.class);
            startActivity(intent);
        });

        binding.Activity2Button.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, MainActivity2.class);
            startActivity(intent);
        });

    }
}