package com.iesportada.semipresencial.hlcto03.Activity2;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.iesportada.semipresencial.hlcto03.R;
import com.iesportada.semipresencial.hlcto03.databinding.ActivityMain2Binding;

import java.io.IOException;
import java.net.URL;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MainActivity2 extends AppCompatActivity {
    private ActivityMain2Binding binding;
    private double rate;
    private double dolarAmount;
    private double euroAmount;

    private static final String MYURL = "https://rnarvaiza.me/files/rate.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        binding = ActivityMain2Binding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        View view = binding.getRoot();
        setContentView(view);
        OkHTTPCurrencyRateDownload();
    }

    public void onRadioButtonFromDollarToEuroClicked(View view){
        if(binding.radioButtonDolar2Euro.isChecked()){
            binding.editTextDolar.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    try{
                        if (binding.editTextDolar.hasFocus()){
                            setDolarAmount(Double.valueOf(binding.editTextDolar.getText().toString()));
                            binding.editTextEuro.setText(fromDolarToEuro());
                        }
                    } catch (NumberFormatException nfe){
                        showToast(nfe.getMessage());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }


    public void onRadioButtonFromEuroToDollarClicked(View view){
        if (binding.radioButtonEuro2Dolar.isChecked()){
            binding.editTextEuro.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    try{
                        if (binding.editTextEuro.hasFocus()){
                            setEuroAmount(Double.valueOf(binding.editTextEuro.getText().toString()));
                            binding.editTextDolar.setText(fromEuroToDolar());
                        }
                    }catch (NumberFormatException nfe){
                        showToast(nfe.getMessage());
                    }
                }
                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }


    private void OkHTTPCurrencyRateDownload() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(MYURL)
                .build();
        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if(response.isSuccessful()){
                    String usdRate = response.body().string();
                    MainActivity2.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                setRate(Double.parseDouble(usdRate));
                            }catch (Exception e){
                                showToast(e.getMessage());
                            }
                        }
                    });
                }
            }
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("Error", e.getMessage());
                showToast(e.getMessage());
            }
        });
    }

    public String fromDolarToEuro(){
        String result = Double.toString((getRate())*(Double.valueOf(getDolarAmount())));

        return result;
    }

    public String fromEuroToDolar(){
        String result = Double.toString(((Double.valueOf(getEuroAmount()))/getRate()));
        return result;
    }


    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }


    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getDolarAmount() {
        return dolarAmount;
    }

    public void setDolarAmount(double dolarAmount) {
        this.dolarAmount = dolarAmount;
    }

    public double getEuroAmount() {
        return euroAmount;
    }

    public void setEuroAmount(double euroAmount) {
        this.euroAmount = euroAmount;
    }
}