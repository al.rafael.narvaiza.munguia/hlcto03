package com.iesportada.semipresencial.hlcto03.Activity1.Network;

import android.graphics.Bitmap;

public class Result {
    private int code; //indica el código de estado devuelto por el servidor web
    private String message; //información del error
    private String content; //fichero descargado
    private Bitmap image;
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}