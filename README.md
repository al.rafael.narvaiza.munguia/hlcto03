***Tarea online 3 HLC***

**Ejercicio1**
Diseñamos una vista conteniendo un editText y un webView, acompañado de un imageView.
A continuación se procede a lanzar dos métodos que cada uno va a desencadenar el uso de dos tareas asyncronas diferentes.
La primera tarea asincrona tendrá por objeto cargar en el webView o imageView aquello que pasemos por la url.
La seguda de estas tareas tiene por objeto realizar la descarga del jpg/png/txt/html.

**Ejercicio2**
Se procede a reproducir el conversor con mejora en el que simplemente escribiendo en los editText se va obteniendo el cambio.
Se levanta una llamada a OkHttp para realizar la descarga del ratio de conversión.

##
- Todos los archivos están disponibles en : https://rnarvaiza.me/files/
- Existe un video del funcionamiento del Ejercicio1 en youtube: https://youtu.be/BaNszmb1FOU . Aquí se muestra como la aplicación lanzada desde AS funciona en el dispositivo. Después de muchas pruebas, he terminado por desehechar el uso del emulador, ya que a la hora de consumir recursos en servidores https me ha estado dando muchos problemas. Para visualizar el video, que está en modo privado, es necesario autentificarse en google con la cuenta de iesportada.
